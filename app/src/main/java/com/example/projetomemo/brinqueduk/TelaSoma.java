package com.example.projetomemo.brinqueduk;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class TelaSoma extends Activity implements View.OnClickListener, MediaPlayer.OnCompletionListener {
    private ImageView img1, img2;
    private Button res1, res2, res3, reinicio,som,voltar;
    private ArrayList<Apoio> lista;
    private int quemEstaComOCerto;
    private MediaPlayer mp,mp1,mp2;
    private ProgressBar progresso;
    private int progressStatus=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_soma);


        img1 = (ImageView)findViewById(R.id.num1);
        img2 = (ImageView)findViewById(R.id.num2);

        res1 = (Button)findViewById(R.id.btn1);
        res1.setOnClickListener(this);

        res2 = (Button)findViewById(R.id.btn2);
        res2.setOnClickListener(this);

        res3 = (Button)findViewById(R.id.btn3);
        res3.setOnClickListener(this);

        reinicio = (Button)findViewById(R.id.btnRe);
        reinicio.setOnClickListener(this);

        som = (Button)findViewById(R.id.btnSom);
        som.setOnClickListener(this);

        voltar = (Button)findViewById(R.id.btnhome);
        voltar.setOnClickListener(this);

        progresso = (ProgressBar)findViewById(R.id.barradeprogresso);
        progresso.setProgress(progressStatus);

        carregarElementos();
        iniciarJogo();
    }


    public void iniciarJogo(){

        res1.setBackgroundColor(Color.WHITE);
        res2.setBackgroundColor(Color.WHITE);
        res3.setBackgroundColor(Color.WHITE);
        res1.setVisibility(View.VISIBLE);
        res2.setVisibility(View.VISIBLE);
        res3.setVisibility(View.VISIBLE);

        Collections.shuffle(lista); //embaralhar lista

        img1.setImageResource(lista.get(0).getImagem()); //pega o primeiro nome da lista e coloca a imagem 0
        img2.setImageResource(lista.get(1).getImagem());

        Random r = new Random(); //gera aleatório
        Random k = new Random();

        int a,b,res;
        int posicao = r.nextInt(3); //define a quantidade maxima do int

        if(posicao==0){
            a = lista.get(0).getValorImagem();
            b = lista.get(1).getValorImagem();
            res = a+b;
            res1.setText(Integer.toString(res)); //resposta certa pq a imagem é 0

            int val1 = k.nextInt(20);
            int val2;
            do{
                val2 = k.nextInt(20);
            }while(val2 == val1);

            res2.setText(Integer.toString(val1));
            res3.setText(Integer.toString(val2));
            quemEstaComOCerto=1;
        }
        if(posicao==1){
            a = lista.get(0).getValorImagem();
            b = lista.get(1).getValorImagem();
            res = a+b;
            res2.setText(Integer.toString(res)); //resposta certa pq a imagem é 0

            int val1 = k.nextInt(20);
            int val2;
            do{
                val2 = k.nextInt(20);
            }while(val2 == val1);

            res1.setText(Integer.toString(val1));
            res3.setText(Integer.toString(val2));
            quemEstaComOCerto=2;
        }
        if(posicao==2){
            a = lista.get(0).getValorImagem();
            b = lista.get(1).getValorImagem();
            res = a+b;

            int val1 = k.nextInt(20);
            int val2;
            do{
                val2 = k.nextInt(20);
            }while(val2 == val1);

            res3.setText(Integer.toString(res)); //resposta certa pq a imagem é 0
            res2.setText(Integer.toString(val1));
            res1.setText(Integer.toString(val2));
            quemEstaComOCerto=3;
        }
    }

    public void carregarElementos(){
        lista = new ArrayList <Apoio>();

        Apoio a = new Apoio();
        a.setImagem(R.drawable.one);
        a.setValorImagem(1);
        a.setSomImagem(1);
        lista.add(a);

        Apoio b = new Apoio();
        b.setImagem(R.drawable.two);
        b.setValorImagem(2);
        b.setSomImagem(2);
        lista.add(b);

        Apoio c = new Apoio();
        c.setImagem(R.drawable.three);
        c.setValorImagem(3);
        c.setSomImagem(3);
        lista.add(c);

        Apoio d = new Apoio();
        d.setImagem(R.drawable.four);
        d.setValorImagem(4);
        d.setSomImagem(4);
        lista.add(d);

        Apoio e = new Apoio();
        e.setImagem(R.drawable.five);
        e.setValorImagem(5);
        e.setSomImagem(5);
        lista.add(e);

        Apoio f = new Apoio();
        f.setImagem(R.drawable.six);
        f.setValorImagem(6);
        f.setSomImagem(6);
        lista.add(f);

        Apoio g = new Apoio();
        g.setImagem(R.drawable.seven);
        g.setValorImagem(7);
        g.setSomImagem(7);
        lista.add(g);

        Apoio h = new Apoio();
        h.setImagem(R.drawable.nine);
        h.setValorImagem(9);
        h.setSomImagem(9);
        lista.add(h);
    }


    public int devolverMidia(int x) {
        if (x == 1) {
            return R.raw.um;
        }
        if (x == 2) {
            return R.raw.dois;
        }
        if (x == 3) {
            return R.raw.tres;
        }
        if (x == 4) {
            return R.raw.quatro;
        }
        if (x == 5) {
            return R.raw.cinco;
        }
        if (x == 6) {
            return R.raw.seis;
        }
        if (x == 7) {
            return R.raw.sete;
        }
        if (x == 8) {
            return R.raw.oito;
        }
        if (x == 9) {
            return R.raw.nove;
        }
        if (x == 11) {
            return R.raw.aplauso;
        }
        if (x == 12){
            return R.raw.mais;
        }
        return 0;
    }

    public void onClick(View view) {

        if(view == voltar)
        {
            Intent i = new Intent(this, TelaOpcMat.class);
            startActivity(i);
            /*mp.pause();
            mp1.pause();
            mp2.pause();*/
        }
        if (view == reinicio){
            iniciarJogo();
        }

        if (view == res1){
            if(quemEstaComOCerto == 1){
                mp = new MediaPlayer();
                mp.setOnCompletionListener(this);
                mp = MediaPlayer.create(this,devolverMidia(11));
                mp.start();
                res1.setBackgroundColor(Color.GREEN);
                int x = progresso.getProgress();
                if(x >=100) {
                    Intent i = new Intent(this, TelaParabens.class);
                    startActivity(i);
                } else {
                    x = x+10;
                    progresso.setProgress(x);
                    iniciarJogo();
                }
            }else{
                res1.setVisibility(View.INVISIBLE);
            }
        }

        if (view == res2){
            if(quemEstaComOCerto == 2){
                mp = new MediaPlayer();
                mp.setOnCompletionListener(this);
                mp = MediaPlayer.create(this,devolverMidia(11));
                mp.start();
                res2.setBackgroundColor(Color.GREEN);
                int x = progresso.getProgress();
                if(x >=100) {
                    Intent i = new Intent(this, TelaParabens.class);
                    startActivity(i);
                } else {
                    x = x+10;
                    progresso.setProgress(x);
                    iniciarJogo();
                }
            }else{
                res2.setVisibility(View.INVISIBLE);
            }
        }

        if (view == res3){
            if(quemEstaComOCerto == 3){
                mp = new MediaPlayer();
                mp.setOnCompletionListener(this);
                mp = MediaPlayer.create(this,devolverMidia(11));
                mp.start();
                res3.setBackgroundColor(Color.GREEN);
                int x = progresso.getProgress();
                if(x >=100) {
                    Intent i = new Intent(this, TelaParabens.class);
                    startActivity(i);
                } else {
                    x = x+10;
                    progresso.setProgress(x);
                    iniciarJogo();
                }
            }else{
                res3.setVisibility(View.INVISIBLE);
            }
        }

        if (view == som){

            mp = new MediaPlayer();
            mp.setOnCompletionListener(this);
            mp = MediaPlayer.create(this,devolverMidia((lista.get(0).getSomImagem())));

            Handler h = new Handler();
            h.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mp.start();
                }
            }, 200);


            mp1 = new MediaPlayer();
            mp1.setOnCompletionListener(this);
            mp1 = MediaPlayer.create(this, R.raw.mais);
            Handler h1 = new Handler();
            h1.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mp1.start();
                }
            }, 1000);


            mp2 = new MediaPlayer();
            mp2.setOnCompletionListener(this);
            mp2 = MediaPlayer.create(this,devolverMidia((lista.get(1).getSomImagem())));
            Handler h2 = new Handler();
            h2.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mp2.start();
                }
            }, 1400);


        }
    }


    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        mp.release();
    }
}
