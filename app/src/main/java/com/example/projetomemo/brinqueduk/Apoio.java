package com.example.projetomemo.brinqueduk;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Apoio {

    private int imagem;
    private int valorImagem;

    public int getSomImagem() {
        return somImagem;
    }

    public void setSomImagem(int somImagem) {
        this.somImagem = somImagem;
    }

    private int somImagem;

    public int getImagem() {
        return imagem;
    }

    public void setImagem(int imagem) {
        this.imagem = imagem;
    }

    public int getValorImagem() {
        return valorImagem;
    }

    public void setValorImagem(int valorImagem) {
        this.valorImagem = valorImagem;
    }
}
