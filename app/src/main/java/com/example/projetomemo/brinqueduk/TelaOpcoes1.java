package com.example.projetomemo.brinqueduk;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TelaOpcoes1 extends AppCompatActivity  implements View.OnClickListener{


    private Button btn,btn2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_opcoes1);

        btn = (Button)findViewById(R.id.portugues);
        btn.setOnClickListener(this);
        btn2 = (Button)findViewById(R.id.matematica);
        btn2.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {

        if(view == btn2){

            Intent i = new Intent(this, TelaOpcMat.class);
            startActivity(i);
        }

        if(view == btn){

            Intent i = new Intent(this, TelaOpcPort.class);
            startActivity(i);
        } }
}
