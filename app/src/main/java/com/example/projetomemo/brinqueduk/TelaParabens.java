package com.example.projetomemo.brinqueduk;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TelaParabens extends AppCompatActivity implements View.OnClickListener {

    private Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_parabens);

        btn = (Button)findViewById(R.id.Inicio);
        btn.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        if(view == btn){
            Intent i = new Intent(this, TelaOpcMat.class);
            //Bundle b = new Bundle();
            startActivity(i);
        }
    }
}
